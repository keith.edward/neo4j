import numpy as np

import matplotlib
matplotlib.use('TkAgg')


health_score = [23.0,86.0,2.0,6.0]
health_score_tenure = [5.909830937252133,22.215327945430275,0.531745171349704,6.956206046895403]
months_employed = [58,52,51,46]

print("Health Score")
print(np.corrcoef(health_score, months_employed))

print("Health Score Tenure")
print(np.corrcoef(health_score_tenure, months_employed))

