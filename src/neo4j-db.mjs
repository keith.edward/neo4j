import * as neo4j from 'neo4j-driver';

const {
	NEO4J_CONNECTION_STRING,
	NEO4J_USERNAME,
	NEO4J_PASSWORD
} = process.env;

let driver = null;

async function runAsyncCreateUserQuery(user) {
	const id = user._ID;
	const session = driver.session(); // TODO Only create session when needed. Try an earlier filter.
	const firstName = user.FIRST_NAME;
	const lastName = user.LAST_NAME;
	const displayName = `${firstName} ${lastName}`;
	const deletedAt = user.DELETED_AT__TI;
	const createdAt = user.CREATED_AT;
	try {
		const query = 'CREATE (a:Person {user_id: $user_id, first_name: $first_name, last_name: $last_name, name: $name, deleted_at: $deleted_at, created_at: $created_at}) RETURN a';
		console.log(firstName,lastName,createdAt,createdAt.toJSON,id);
		if (firstName!== null
			&& lastName !== null
			&& createdAt !== null
			&& createdAt.toJSON
			&& id !== null) {
			const parameters = {
				first_name: firstName,
				last_name: lastName,
				name: displayName,
				user_id: id.toString(),
				deleted_at: deletedAt ? deletedAt.toJSON() : null,
				created_at: createdAt.toJSON()
			};
			await session.run(query, parameters);
		}
	} finally {
		await session.close();
	}
}

async function buildUserNodes(userCollection) {
	const queryPromises = [];
	for (let user of userCollection) {
		queryPromises.push(runAsyncCreateUserQuery(user));
	}
	return Promise.all(queryPromises);
}

async function runAsyncCreateBonusRelationshipQuery(bonus) {
	const {
		GIVER_ID: giver_id,
		RECEIVER_ID : receiver_id,
		AMOUNT : amount,
		REASON: reason,
		NTH_MONTH: nth_month,
		CREATED_AT: created_at } = bonus;
	if (giver_id && receiver_id && created_at && amount > 0) {
		const session = driver.session();
		try {
			const query = `MATCH (a:Person), (b:Person) 
			WHERE a.user_id = $giver_id AND b.user_id = $receiver_id 
			CREATE (a)-[:BONUS_GIVEN { giver_id : $giver_id, receiver_id : $receiver_id, amount: $amount, reason: $reason, nth_month: $nth_month, created_at: $created_at}]->(b)`;
			const parameters = { giver_id, receiver_id, amount, reason, nth_month, created_at: created_at.toJSON() };
			await session.run(query, parameters);
		} catch (e) {
			console.log(e);
		} finally {
			await session.close();
		}
	}
}

// eslint-disable-next-line no-unused-vars
async function buildBonusCollection (bonusCollection) {
	for (let bonus of bonusCollection) {
		await runAsyncCreateBonusRelationshipQuery(bonus);
	}
}


async function runAsyncCreateMonthlyBonusRelationshipQuery(bonus) {
	const {
		GIVER_ID: giver_id,
		RECEIVER_ID : receiver_id,
		MONTH: month,
		NTH_MONTH: nth_month,
		NUM_BONUS_GIVEN: num_bonus_given,
		TOTAL_AMOUNT : total_amount } = bonus;
	if (giver_id, receiver_id, month) {
		const session = driver.session();
		try {
			const query = `MATCH (a:Person), (b:Person) 
			WHERE a.user_id = $giver_id AND b.user_id = $receiver_id 
			CREATE (a)-[:MONTHLY_BONUS { giver_id : $giver_id, receiver_id : $receiver_id, month: $month, nth_month: $nth_month, total_amount: $total_amount, num_bonus_given: $num_bonus_given}]->(b)`;
			const parameters = {
				giver_id,
				receiver_id,
				total_amount,
				num_bonus_given,
				nth_month,
				strMonth: month.toJSON().replace('-', '').split('-')[0],
				month: month.toJSON() };
			await session.run(query, parameters);
		} catch (e) {
			console.log(e);
		} finally {
			await session.close();
		}
	}
}

// eslint-disable-next-line no-unused-vars
async function buildMonthlyBonusCollection (monthlyBonusCollection) {
	for (let bonus of monthlyBonusCollection) {
		await runAsyncCreateMonthlyBonusRelationshipQuery(bonus);
	}
}

// Public: Put all exports below this line.

/**
 * Builds a social graph in Neo4J database.
 * @param userCollection
 * @param weightedConnections
 * @param nonWeightedConnections
 * @returns {Promise<void>}
 */
export async function buildSocialGraph({
	userCollection,
	monthlyBonusCollection
}) {
	driver = neo4j.driver(NEO4J_CONNECTION_STRING, neo4j.auth.basic(NEO4J_USERNAME, NEO4J_PASSWORD));
	await buildUserNodes(userCollection);
	await buildMonthlyBonusCollection(monthlyBonusCollection);
	await driver.close();
}

