import math
from string import Template
import numpy as np
from neo4j import GraphDatabase
from tabulate import tabulate


class HealthScore:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_results(self):
        with self.driver.session() as session:
            month = "2022-05"
            nth_month = "4.0"

            self._print_month(self, session, month)
            #self._print_nth_month(self, nth_month, session)


            #session.execute_write(self._get_health_scores, month, "betweenness")
            #session.execute_write(self._get_health_scores, month, "articleRank")
            #page_rank_data = session.execute_write(self._get_health_scores, month, "pageRank")


    @staticmethod
    def _print_nth_month(self, nth_month, session):
        try:
            session.execute_write(self._drop_project_graph, nth_month)
        except Exception as inst:
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)
            print("Can't drop Graph. It might be a first time run.")

        print("Compute the nth month")
        session.execute_write(self._project_graph_nth_month, nth_month)
        non_weight_data_nth = session.execute_write(self._get_health_scores, nth_month, "degree")
        weight_data_nth = session.execute_write(self._get_health_scores, nth_month, "degree", ",{ relationshipWeightProperty: 'weight' }")
        k_shell_data_nth = self._get_k_shell(non_weight_data_nth, weight_data_nth, nth_month)
        dataNthMatrix =  self._get_data_matrix(k_shell_data_nth, weight_data_nth)
        print(tabulate(dataNthMatrix))

    @staticmethod
    def _print_month(self, session, month):
        try:
            session.execute_write(self._drop_project_graph, month)
        except Exception as inst:
            print(type(inst))  # the exception instance
            print(inst.args)  # arguments stored in .args
            print(inst)
            print("Can't drop Graph. It might be a first time run.")
            print("")
            print("")
        session.execute_write(self._project_graph_month, month)


        print("Health Score for " + month + " correlated against total days employed")
        print("")
        template_string = """
            CALL gds.$algorithm.stream('Bonusly-$month'$options)
            YIELD nodeId, score
            WHERE gds.util.asNode(nodeId).user_id IS NOT NULL and score > 0 AND
                  duration.inMonths(date(LEFT(gds.util.asNode(nodeId).created_at,10)), date("$month")).months > 1 AND
                  duration.inMonths(date("$month"), date(LEFT(gds.util.asNode(nodeId).deleted_at,10))).months > 0
            RETURN gds.util.asNode(nodeId).user_id AS user_id, 
                   gds.util.asNode(nodeId).name AS name, 
                   score AS healthScore,
                   score / log(duration.inMonths(date(LEFT(gds.util.asNode(nodeId).created_at,10)), date("$month")).months + 12)  AS healthScoreTenure,
                   gds.util.asNode(nodeId).created_at AS created_at, 
                   gds.util.asNode(nodeId).deleted_at AS deleted_at,
                   duration.inMonths(date(LEFT(gds.util.asNode(nodeId).created_at,10)), date(LEFT(gds.util.asNode(nodeId).deleted_at,10))).months AS monthsEmployed,
                   duration.inMonths(date(LEFT(gds.util.asNode(nodeId).created_at,10)), date("$month")).months AS  monthsWorked,
                   duration.inMonths(date("$month"), date(LEFT(gds.util.asNode(nodeId).deleted_at,10))).months AS  monthsLeft                   
            ORDER BY created_at
        """
        template = Template(template_string)

        print("Degree")
        non_weight_query = template.substitute(month = month, algorithm = "degree", options = '')
        non_weight_data = session.execute_write(self._get_health_scores, non_weight_query, "degree")

        print("Degree with weight")
        weight_query = template.substitute(month = month, algorithm = "degree", options = ",{ relationshipWeightProperty: 'weight' }")
        print("Weight query")
        print(weight_query)
        weight_data = session.execute_write(self._get_health_scores, weight_query, "Degree for month: " + month)

        print("K Shell")
        k_shell_data = self._get_k_shell(non_weight_data, weight_data, month)

        dataNthMatrix =  self._get_data_matrix(k_shell_data, weight_data)
        print(tabulate(dataNthMatrix))

    @staticmethod
    def _get_data_matrix(k_shell_data, weight_data):
        orderedNames = ['user_id','name','monthsEmployed', 'created_at', 'deleted_at']
        dataMatrix = [orderedNames]
        for weight_person in weight_data:
            dataMatrix.append([weight_person[i] for i in orderedNames])

        index = 0
        while index < len(dataMatrix):
            dataMatrix[index].append(k_shell_data[index])
            index += 1

        return dataMatrix

    @staticmethod
    def _get_health_scores(tx, query, title):
        result = tx.run(query)
        data = result.data()
        health_scores = []
        health_scores_tenure = []
        months_employed = []
        months_left = []
        for person in data:
            if person['deleted_at'] is not None:
                health_scores.append(person['healthScore'])
                health_scores_tenure.append(person['healthScoreTenure'])
                months_employed.append(person['monthsEmployed'])
                months_left.append(person['monthsLeft'])

        if (len(health_scores) > 2):
            print("Months Employed")
            print(np.corrcoef(health_scores, months_employed))
            print("Months Left")
            print(np.corrcoef(health_scores, months_left))
        print("")
        print("")
        print("")

        return data

    @staticmethod
    def _get_k_shell(non_weight_data, weight_data, month):
        if len(non_weight_data) == len(weight_data):
            total_health_score = []
            months_employed = []
            months_left = []
            index = 0
            all_scores = ["K-Shell for month:" + month]
            while index < len(non_weight_data):
                non_weight_person = non_weight_data[index]
                weight_person = weight_data[index]
                if non_weight_person["user_id"] != weight_person["user_id"]:
                    raise Exception("Persons do not match. There is an error in the data.")

                weight_score = weight_person['healthScore']
                non_weight_score = non_weight_person['healthScore']

                if weight_score < non_weight_score:
                     raise Exception("Non weight score must be less than weight score")

                months_worked =  weight_person['monthsWorked']
                if months_worked is None:
                    raise Exception("Null Months worked")
                k_shell_score = math.sqrt(weight_score * non_weight_score) / math.log(months_worked + 12)

                all_scores.append(k_shell_score)
                if non_weight_person['deleted_at'] is not None:
                     total_health_score.append(k_shell_score)
                     months_left.append(weight_person['monthsLeft'])
                     months_employed.append(weight_person['monthsEmployed'])

                index += 1
            if (len(total_health_score) > 2):
                print("Employed")
                print(np.corrcoef(total_health_score, months_employed))
                print("Left")
                print(np.corrcoef(total_health_score, months_left))
            return all_scores

    @staticmethod
    def _drop_project_graph(tx, suffix):
        template_string = "CALL gds.graph.drop('Bonusly-$suffix')"
        template = Template(template_string)
        query = template.substitute(suffix = suffix)
        tx.run(query)

    @staticmethod
    def _project_graph_month(tx, month):
        template_string = """
            CALL gds.graph.project.cypher(
                'Bonusly-$month',
                'MATCH (n:Person) RETURN id(n) AS id',
                'MATCH (n:Person)-[r:MONTHLY_BONUS]->(m:Person) WHERE r.month CONTAINS "$month" RETURN r.num_bonus_given AS weight, id(n) AS source, id(m) AS target')
            YIELD
            graphName AS graph, nodeQuery, nodeCount AS nodes, relationshipQuery, relationshipCount AS rels"""
        template = Template(template_string)
        query = template.substitute(month = month)
        tx.run(query)

    @staticmethod
    def _project_graph_nth_month(tx, nth_month):
        template_string = """
            CALL gds.graph.project.cypher(
                'Bonusly-$nth_month',
                'MATCH (n:Person) RETURN id(n) AS id',
                'MATCH (n:Person)-[r:MONTHLY_BONUS]->(m:Person) WHERE r.nth_month = $nth_month RETURN r.num_bonus_given AS weight, id(n) AS source, id(m) AS target')
            YIELD
            graphName AS graph, nodeQuery, nodeCount AS nodes, relationshipQuery, relationshipCount AS rels"""
        template = Template(template_string)
        query = template.substitute(nth_month = nth_month)
        tx.run(query)


if __name__ == "__main__":
    health_score = HealthScore(
        "bolt://localhost:7687",
        "neo4j",
        "test")

    health_score.print_results()
    health_score.close()

