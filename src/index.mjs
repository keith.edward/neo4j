import { getUserCollection, getMonthlyBonusCollection } from './snoflake-db.mjs';

import { buildSocialGraph } from './neo4j-db.mjs';

const userCollection = await getUserCollection();
const monthlyBonusCollection = await getMonthlyBonusCollection();

buildSocialGraph({
	userCollection,
	monthlyBonusCollection,
}); // Ignore promise returned.











