import numpy as np
import pandas as pd
from neo4j import GraphDatabase


class HealthScore:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_degree_centality(self, month):
        with self.driver.session() as session:
            session.execute_write(self._drop_project_graph, month)
            session.execute_write(self._project_graph, month)
            return session.execute_write(self._get_degree_centrality, month)

    @staticmethod
    def _drop_project_graph(tx, month):
        tx.run(f"""
            CALL gds.graph.drop('Bonusly-{month}', false)""")

    @staticmethod
    def _project_graph(tx, month):
        tx.run(f"""
            CALL gds.graph.project.cypher(
                'Bonusly-{month}',
                'MATCH (n:Person) RETURN id(n) AS id',
                'MATCH (n:Person)-[r:MONTHLY_BONUS]->(m:Person) WHERE r.month CONTAINS "{month}" RETURN r.num_bonus_given AS weight, id(n) AS source, id(m) AS target')
            YIELD
            graphName AS graph, nodeQuery, nodeCount AS nodes, relationshipQuery, relationshipCount AS rels""")

    @staticmethod
    def _get_degree_centrality(tx, month):
        result = tx.run(f"""
            CALL gds.degree.stream('Bonusly-{month}')
            YIELD nodeId, score
            WHERE gds.util.asNode(nodeId).user_id IS NOT NULL and score > 0 
            RETURN gds.util.asNode(nodeId).user_id AS user_id, 
                   gds.util.asNode(nodeId).name AS name, 
                   score AS healthScore,
                   gds.util.asNode(nodeId).created_at AS created_at, 
                   gds.util.asNode(nodeId).deleted_at AS deleted_at,
                   duration.inDays(date(LEFT(gds.util.asNode(nodeId).created_at,10)), 
                   date(LEFT(gds.util.asNode(nodeId).deleted_at,10))).days AS daysEmployeed,
                   duration.inDays(date(LEFT(gds.util.asNode(nodeId).deleted_at,10)), date()).days AS  daysLeft
            ORDER BY daysEmployeed DESC, name DESC""")
        data = result.data()
        user_id = []
        name = []
        created_at = []
        deleted_at = []
        health_score = []
        days_employed = []
        days_left = []
        months = []
        print("Total Data Set")
        print(len(data))
        for person in data:
            if (person['deleted_at'] is not None) and (month not in person['deleted_at']):
                user_id.append(person['user_id'])
                name.append(person['name'])
                months.append(month)
                created_at.append(person['created_at'])
                deleted_at.append(person['deleted_at'])
                health_score.append(person['healthScore'])
                days_employed.append(person['daysEmployeed'])
                days_left.append(person['daysLeft'])

        print(f"Health Score for {month}")
        print(len(health_score))
        print("Degree Centrality - Total Days")
        print(np.corrcoef(health_score, days_employed))

        print("Degree Centrality - Days Left")
        print(np.corrcoef(health_score, days_left))
        return user_id, name, month, created_at, deleted_at, health_score, days_employed, days_left


if __name__ == "__main__":
    greeter = HealthScore("bolt://localhost:7687", "neo4j", "test")
    months = ['2021-06', '2021-07', '2021-08','2021-09', '2021-10', '2021-11', '2021-12',
              '2022-01', '2022-02', '2022-03', '2022-04', '2022-05', '2022-06', '2022-07', '2022-08', '2022-09']
    column_names = ['user_id', 'name', 'month', 'created_at', 'deleted_at', 'health_score', 'days_employed', 'days_left']
    monthly_health_score = pd.DataFrame(columns = column_names)
    for month in months:
        user_id, name, month, created_at, deleted_at, health_score, days_employed, days_left = greeter.print_degree_centality(month)
        temp_monthly_health_score = pd.DataFrame(
            {
                'user_id': user_id,
                'name': name,
                'month': month,
                'created_at': created_at,
                'deleted_at': deleted_at,
                'health_score': health_score,
                'days_employed': days_employed,
                'days_left': days_left
            }
        )
        monthly_health_score = pd.concat([monthly_health_score, temp_monthly_health_score])
        
    monthly_health_score.head(100)
    monthly_health_score.to_csv('monthly_health_score.csv')    
    greeter.close()
