import snowflake from 'snowflake-sdk';

const {
	SNOWFLAKE_ACCOUNT,
	SNOWFLAKE_USERNAME,
	SNOWFLAKE_PASSWORD,
	SNOWFLAKE_APPLICATION,
	SNOWFLAKE_ROLE,
	SNOWFLAKE_WAREHOUSE,
	SNOWFALKE_DATABASE,
	SNOWFLAKE_SCHEMA,
	COMPANY_ID
} = process.env;

// Create a Connection object that we can use later to connect.
const connection = snowflake.createConnection({
	account: SNOWFLAKE_ACCOUNT,
	username: SNOWFLAKE_USERNAME,
	password: SNOWFLAKE_PASSWORD,
	application: SNOWFLAKE_APPLICATION,
	role: SNOWFLAKE_ROLE,
	warehouse: SNOWFLAKE_WAREHOUSE,
	database: SNOWFALKE_DATABASE,
	schema: SNOWFLAKE_SCHEMA
});



// Try to connect to Snowflake, and check whether the connection was successful.
connection.connect(
	function(err, conn) {
		if (err) {
			console.error('Unable to connect: ' + err.message);
			console.error('Conn' + conn);
		}
		else {
			console.log(`Successfully connected to Snowflake. Connection ID ${conn.getId()}`);
			// Optional: store the connection ID.
			//connection_ID = conn.getId();
		}
	}
);

export async function getUserCollection () {
	return new Promise((resolve, reject) => {
		connection.execute({
			sqlText: `select * from users_view where user_mode = 'normal' and company_id = '${COMPANY_ID}'
					and not contains(email ,'+') 
					and contains(email, 'bonus')
					and last_name is not null
					and first_name is not null 
					and _id NOT IN ('50aa452af667111519000024', '5665b736a96b81ca96000001', '551e96c104612414f700002e', '5665b7375a203fa93b000001', '5665b737efd5901353000001',
					'5667314da0e448d6c1000001', '56673320f6374526d0000001', '56673405b4bf68dfe2000001', '5669a7fe5e516cc029000001', '568d3c189520b12ad0000001',
					'568d3c1a1e2357524b000001', '5d431ecac2515b000b8bca95', '5d431f26c2515b000b8bca9c', '5d431f4cc2515b000b8bcaa2', '5d431f6fc2515b000b8bcaa8',
					'5c992ec23b2076000b2f0f1d', '52eda094d580fd8fd7000007', '534050b64432b70bb5000001', '55a803e3ddf13a099b000003', '5a4677f2a56d9d394d8d9b75',
					'5d7edb6083aeff0044cbfa97', '5f90ae098bd8650054002b35', '6119b4855916290069a92200', '6164d8feff1618006b6de194', '55e8b5ab4f182f529f000015',
					'56b8d18d953398bc4b000002', '612c2b9f928c7b006838ce27', '53445c771222fd478f000001', '602f1381e78df000b33a7a84', '60edcb5c9a833d00d8060a0f')`,
			complete: function(err, stmt, rows) {
				if (err) {
					console.error('Failed to execute statement due to the following error: ' + err.message);
					reject(err);
				} else {
					console.log('Successfully executed statement: ' + stmt.getSqlText());
					resolve(rows);
				}
			}
		});
	});
}

export async function getBonusCollection () {
	return new Promise((resolve, reject) => {
		connection.execute({
			sqlText: `select * from hackathon_bonus where bot_given = false and company_id = '${COMPANY_ID}' and amount > 0  and giver_id != receiver_id`,
			complete: function(err, stmt, rows) {
				if (err) {
					console.error('Failed to execute statement due to the following error: ' + err.message);
					reject(err);
				} else {
					console.log('Successfully executed statement: ' + stmt.getSqlText());
					resolve(rows);
				}
			}
		});
	});
}

export async function getMonthlyBonusCollection () {
	return new Promise((resolve, reject) => {
		connection.execute({
			sqlText: `select
                          last_day(created_at) as month, giver_id, receiver_id, nth_month,
			count(_id) as num_bonus_given, sum(amount) as total_amount
                      from hackathon_bonus
                      where bot_given = false
                        and company_id = '${COMPANY_ID}'
                        and amount > 0
                        and giver_id != receiver_id
                      group by last_day(created_at), giver_id, receiver_id, nth_month;`,
			complete: function(err, stmt, rows) {
				if (err) {
					console.error('Failed to execute statement due to the following error: ' + err.message);
					reject(err);
				} else {
					console.log('Successfully executed statement: ' + stmt.getSqlText());
					resolve(rows);
				}
			}
		});
	});
}





