var test = [{
	'user_id': '6331570813591b0001417c0c',
	'name': 'Hailey Bugbee',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-09-26 07:38:48.126 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '633156eb762485000142eed8',
	'name': 'Ian Stuchlik',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-09-26 07:38:19.861 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '633156d508033100018e0238',
	'name': 'Megan Franz',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-09-26 07:37:57.714 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '631ee268a1d264000166a76d',
	'name': 'Andy Lucich',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-09-12 07:40:24.093 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '631ee21be4d3390001203832',
	'name': 'Reina Pinto',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-09-12 07:39:07.161 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '630d360bbfd0fa00012d37c3',
	'name': 'Samantha Calder',
	'healthScore': 0.2603776466856708,
	'created_at': '2022-08-29 21:56:27.353 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62f9fabfa3c9d4006e06783e',
	'name': 'Jensen Means',
	'healthScore': 0.93861064257034,
	'created_at': '2022-08-15 07:50:23.540 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62f9f99ca3c9d4006f3532cb',
	'name': 'Ryan Whitehead',
	'healthScore': 0.9771303485151992,
	'created_at': '2022-08-15 07:45:32.457 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62f9f82ea3c9d4006f35322c',
	'name': 'Matthew Sells',
	'healthScore': 1.2192672761910428,
	'created_at': '2022-08-15 07:39:26.812 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62f9f82da3c9d4006f353224',
	'name': 'Kayla Emerson',
	'healthScore': 0.9391531011336824,
	'created_at': '2022-08-15 07:39:25.458 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62e783879654840071a32f9a',
	'name': 'Eunice Cho',
	'healthScore': 1.2249704189935482,
	'created_at': '2022-08-01 07:40:55.822 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62e78327e5d41f006d356912',
	'name': 'Em Shank',
	'healthScore': 1.5216492631714968,
	'created_at': '2022-08-01 07:39:19.800 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62de4b44831ff900714e1431',
	'name': 'Amani Phipps',
	'healthScore': 0.8882854795805014,
	'created_at': '2022-07-25 07:50:28.861 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62d50e25e8a9260072e32c98',
	'name': 'Alexandria Buvalic',
	'healthScore': 0.37438085150166717,
	'created_at': '2022-07-18 07:39:17.513 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62c4b07f23cf24006dfd712c',
	'name': 'Lucas Greenberg',
	'healthScore': 0.5635967849529117,
	'created_at': '2022-07-05 21:43:27.653 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62c4b07423cf24007071c7eb',
	'name': 'David Brown',
	'healthScore': 0.6778567221576783,
	'created_at': '2022-07-05 21:43:16.357 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62c3eb3b5d0f9200719429f8',
	'name': 'Alessandra Adorisio',
	'healthScore': 0.9121584281811506,
	'created_at': '2022-07-05 07:41:47.772 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '62b1758340964d0070e576ec',
	'name': 'Mariness Didulo',
	'healthScore': 0.5463681618636623,
	'created_at': '2022-06-21 07:38:43.571 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '629db014d839a90070170761',
	'name': 'Sagar Desai',
	'healthScore': 0.8385646670930597,
	'created_at': '2022-06-06 07:43:16.815 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '629daf3cd839a9007174f115',
	'name': 'Kyndall Elliott',
	'healthScore': 0.5599603157690781,
	'created_at': '2022-06-06 07:39:40.339 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '629daf04d839a9007174f10c',
	'name': 'Rachael McCarthy',
	'healthScore': 0.7266855276477268,
	'created_at': '2022-06-06 07:38:44.530 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '628b3af29e37330072147d7a',
	'name': 'Nguyen Le',
	'healthScore': 0.38462676479296903,
	'created_at': '2022-05-23 07:42:42.745 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6282000767481900729dbd26',
	'name': 'Bassey Offiong',
	'healthScore': 0.5169555499693046,
	'created_at': '2022-05-16 07:40:55.572 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6278c4fad60090006e26be2c',
	'name': 'Ryan Parker',
	'healthScore': 0.4186595897769333,
	'created_at': '2022-05-09 07:38:34.595 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6266521d0e62dc006e248e85',
	'name': 'Jonathan Jackson',
	'healthScore': 0.7993443989179794,
	'created_at': '2022-04-25 07:47:41.749 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6266502d0e62dc006c341c28',
	'name': 'Moses Smith',
	'healthScore': 0.7797876081183893,
	'created_at': '2022-04-25 07:39:25.303 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6253dc3e71f9e9007002e609',
	'name': 'Colin Twaddell',
	'healthScore': 0.5731684111413932,
	'created_at': '2022-04-11 07:43:58.716 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '624aa05ea1a6fa006d3bf840',
	'name': 'Mathieu Limousi',
	'healthScore': 0.4436479074297972,
	'created_at': '2022-04-04 07:38:06.420 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6225c520f21c7f006e7d4352',
	'name': 'Andrew Brinkman',
	'healthScore': 0.6863670898290828,
	'created_at': '2022-03-07 08:41:04.709 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '620a1529868402006e6bf34f',
	'name': 'Tyler Dennis',
	'healthScore': 1.3295979634348218,
	'created_at': '2022-02-14 08:39:05.177 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61f7a093a8f863006cbce933',
	'name': 'David Criswell',
	'healthScore': 1.1385508860833593,
	'created_at': '2022-01-31 08:40:51.085 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61f7a058a8f863006d2166fb',
	'name': 'Ryne Gross',
	'healthScore': 0.38462676479296903,
	'created_at': '2022-01-31 08:39:52.916 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61d25412359c98006e0f893d',
	'name': 'Erik Heinz',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-01-03 01:40:34.639 +0000',
	'deleted_at': '2022-07-22 07:12:23.160 +0000',
	'daysEmployed': 200,
	'daysLeft': 76
}, {
	'user_id': '61d25412359c98006e0f893c',
	'name': 'Kylie Bradbury',
	'healthScore': 0.975780893169545,
	'created_at': '2022-01-03 01:40:34.629 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61d25398359c98006d53d498',
	'name': 'Fate Chernoff',
	'healthScore': 0.15000000000000002,
	'created_at': '2022-01-03 01:38:32.614 +0000',
	'deleted_at': '2022-04-23 07:16:45.019 +0000',
	'daysEmployed': 110,
	'daysLeft': 166
}, {
	'user_id': '61b6a5f31b9295006c3f95df',
	'name': 'Michael McEvoy',
	'healthScore': 0.5738016218502234,
	'created_at': '2021-12-13 01:46:27.935 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61b6a50e1b9295006c3f95d4',
	'name': 'Michael Hamel',
	'healthScore': 1.0060553153943959,
	'created_at': '2021-12-13 01:42:39.015 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61b6a4c41b9295006d66f837',
	'name': 'Sheri Williams',
	'healthScore': 0.7484318695019998,
	'created_at': '2021-12-13 01:41:24.148 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61b6a45f1b9295006d66f822',
	'name': 'Ashley Le',
	'healthScore': 0.2792981951073519,
	'created_at': '2021-12-13 01:39:43.373 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61ad6a8e092f3e006bae5dda',
	'name': 'Domenic Palleschi',
	'healthScore': 0.8299268853873021,
	'created_at': '2021-12-06 01:42:38.787 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61ad6a1b092f3e006bae5dcb',
	'name': 'William Ronco',
	'healthScore': 1.7099264957225344,
	'created_at': '2021-12-06 01:40:43.140 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61ad6997092f3e006cdddcca',
	'name': 'Thomas Peterson',
	'healthScore': 0.9419847900485432,
	'created_at': '2021-12-06 01:38:31.082 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bd298306a1006a989609',
	'name': 'Tyler Miller',
	'healthScore': 0.37398949483642985,
	'created_at': '2021-11-15 01:51:37.847 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bcf68306a1006c13606d',
	'name': 'Justin Maki',
	'healthScore': 0.7363562031029484,
	'created_at': '2021-11-15 01:50:46.379 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bcf68306a1006c13606c',
	'name': 'Frederick Allen',
	'healthScore': 1.5957033862027203,
	'created_at': '2021-11-15 01:50:46.368 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bc768306a1006a9895fd',
	'name': 'Alfredo Nolasco Lovo',
	'healthScore': 0.44031702710171133,
	'created_at': '2021-11-15 01:48:38.097 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bc378306a1006c136058',
	'name': 'Jennifer Nguyen',
	'healthScore': 0.37551915818749304,
	'created_at': '2021-11-15 01:47:35.934 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191bb3a8306a1006dd88fcc',
	'name': 'Lisa Schmuttermair',
	'healthScore': 0.7028863145483726,
	'created_at': '2021-11-15 01:43:22.727 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191ba3d8306a1006a9895dc',
	'name': 'Edgar Vargas',
	'healthScore': 1.2886962109534639,
	'created_at': '2021-11-15 01:39:09.103 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6191a9c81a6432006974e50c',
	'name': 'Jack Eshleman',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-11-15 00:28:56.496 +0000',
	'deleted_at': '2022-07-20 07:12:27.593 +0000',
	'daysEmployed': 247,
	'daysLeft': 78
}, {
	'user_id': '61887fdf4352be006b46214d',
	'name': 'Shealagh Coughlin',
	'healthScore': 0.2231046764169921,
	'created_at': '2021-11-08 01:39:43.555 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61887f9b4352be006b462145',
	'name': 'Albany Dawson',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-11-08 01:38:35.370 +0000',
	'deleted_at': '2022-07-09 01:39:11.046 +0000',
	'daysEmployed': 243,
	'daysLeft': 89
}, {
	'user_id': '61887f754352be006b46213d',
	'name': 'Dani Larson',
	'healthScore': 0.6343325936357713,
	'created_at': '2021-11-08 01:37:57.853 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6183ee97cafb580066356afa',
	'name': 'Addis Sisay',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-11-04 14:30:47.901 +0000',
	'deleted_at': '2022-07-21 07:17:50.049 +0000',
	'daysEmployed': 259,
	'daysLeft': 77
}, {
	'user_id': '617f375529f695006c28edbd',
	'name': 'Mili Modi',
	'healthScore': 0.340398114535131,
	'created_at': '2021-11-01 00:39:49.535 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6175ff3891ce7b006959b18e',
	'name': 'Jenna FitzGerald',
	'healthScore': 0.41345337039029717,
	'created_at': '2021-10-25 00:50:01.351 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6175fcfb91ce7b006cd2bdf4',
	'name': 'Mitchell Richey',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-10-25 00:40:27.953 +0000',
	'deleted_at': '2022-03-02 01:29:06.652 +0000',
	'daysEmployed': 128,
	'daysLeft': 218
}, {
	'user_id': '6175fcfb91ce7b006cd2bdf3',
	'name': 'Michelle Keith',
	'healthScore': 0.3977598338589134,
	'created_at': '2021-10-25 00:40:27.945 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6175fcc291ce7b006cd2bde2',
	'name': 'Seunghyun Yoo',
	'healthScore': 0.3838318069908933,
	'created_at': '2021-10-25 00:39:30.906 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6175fc8f91ce7b006cd2bddc',
	'name': 'Murtaza Udaipurwala',
	'healthScore': 0.9966877795167888,
	'created_at': '2021-10-25 00:38:39.807 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6165b1d496ebf301129a091b',
	'name': 'Patrick McCall',
	'healthScore': 0.33640494711983226,
	'created_at': '2021-10-12 16:03:32.991 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6164d915ff161800697f1e4c',
	'name': 'Jeremy Vargas',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-10-12 00:38:45.911 +0000',
	'deleted_at': '2022-04-30 07:14:54.664 +0000',
	'daysEmployed': 200,
	'daysLeft': 159
}, {
	'user_id': '6164d8feff1618006b6de194',
	'name': 'Khyra MacMillan',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-10-12 00:38:22.554 +0000',
	'deleted_at': '2021-10-12 16:00:09.766 +0000',
	'daysEmployed': 0,
	'daysLeft': 359
}, {
	'user_id': '6164d8fdff1618006b6de18d',
	'name': 'Emily Kate Esplin',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-10-12 00:38:21.462 +0000',
	'deleted_at': '2022-05-14 07:17:35.759 +0000',
	'daysEmployed': 214,
	'daysLeft': 145
}, {
	'user_id': '6151136667198c006621fbb8',
	'name': 'Drake Nguyen',
	'healthScore': 0.8647647998017813,
	'created_at': '2021-09-27 00:42:14.998 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6151131b67198c0068e58019',
	'name': 'Joanna De La Via',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-09-27 00:40:59.245 +0000',
	'deleted_at': '2022-07-23 07:13:35.137 +0000',
	'daysEmployed': 299,
	'daysLeft': 75
}, {
	'user_id': '615112bb67198c0069f371de',
	'name': 'Judith Bartels',
	'healthScore': 0.567609066739383,
	'created_at': '2021-09-27 00:39:23.239 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6151129167198c006621fb95',
	'name': 'Liza Brazil',
	'healthScore': 0.922376586198686,
	'created_at': '2021-09-27 00:38:41.260 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6151128f67198c0069f371d6',
	'name': 'Christopher Vaughn',
	'healthScore': 1.2329379468663608,
	'created_at': '2021-09-27 00:38:40.383 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6151128f67198c0067b5c292',
	'name': 'Christopher Weitzel',
	'healthScore': 0.7674477147417055,
	'created_at': '2021-09-27 00:38:39.425 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61422bc0f087e1006893acdf',
	'name': 'David Hundley',
	'healthScore': 0.2825647165617928,
	'created_at': '2021-09-15 17:22:08.193 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6141fb241447bd0069541511',
	'name': 'William Wright',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-09-15 13:54:44.943 +0000',
	'deleted_at': '2022-08-20 07:18:03.977 +0000',
	'daysEmployed': 339,
	'daysLeft': 47
}, {
	'user_id': '6141fb241447bd0069541510',
	'name': 'Tiffany Tran',
	'healthScore': 0.9117440308189406,
	'created_at': '2021-09-15 13:54:44.927 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '613ea212c01fa60068a24158',
	'name': 'Elizabeth McBride',
	'healthScore': 0.7810973984567934,
	'created_at': '2021-09-13 00:57:54.235 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '613ea1b1c01fa60067ccffe9',
	'name': 'Emily Reo',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-09-13 00:56:17.380 +0000',
	'deleted_at': '2022-07-09 02:25:35.319 +0000',
	'daysEmployed': 299,
	'daysLeft': 89
}, {
	'user_id': '613ea17fc01fa60068a2414b',
	'name': 'Malcolm Bressendorff',
	'healthScore': 0.8394218215746753,
	'created_at': '2021-09-13 00:55:28.025 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '613ea0a3c01fa60068a2412f',
	'name': 'Travis Simpson',
	'healthScore': 2.094623760898389,
	'created_at': '2021-09-13 00:51:47.667 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '613e9f7ec01fa60069b4df22',
	'name': 'Emily Wolff',
	'healthScore': 0.6698864877960398,
	'created_at': '2021-09-13 00:46:54.635 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '613e9e72c01fa60069b4df10',
	'name': 'Jonathan Turner',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-09-13 00:42:26.389 +0000',
	'deleted_at': '2022-03-02 01:29:38.139 +0000',
	'daysEmployed': 170,
	'daysLeft': 218
}, {
	'user_id': '613e9dc2c01fa60068a24108',
	'name': 'Nicholas Rak',
	'healthScore': 0.4866499237834552,
	'created_at': '2021-09-13 00:39:30.296 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '612c2b9f928c7b006838ce27',
	'name': 'Alejandro Munguia',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-08-30 00:51:43.426 +0000',
	'deleted_at': '2021-08-30 14:08:16.026 +0000',
	'daysEmployed': 0,
	'daysLeft': 402
}, {
	'user_id': '612c2ae1928c7b0067845506',
	'name': 'Brice Maynard',
	'healthScore': 1.2231324358973361,
	'created_at': '2021-08-30 00:48:34.026 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '612c29cb928c7b006838cdcd',
	'name': 'David Hayoun',
	'healthScore': 1.2644506069510844,
	'created_at': '2021-08-30 00:43:55.991 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6129634bb0c41400664f4253',
	'name': 'Michael Pagano',
	'healthScore': 0.9246365523906347,
	'created_at': '2021-08-27 22:12:27.818 +0000',
	'deleted_at': '2022-10-04 07:21:49.345 +0000',
	'daysEmployed': 403,
	'daysLeft': 2
}, {
	'user_id': '61294f8ab0c4140068abd94e',
	'name': 'Aaron Marsh',
	'healthScore': 0.9900590009171366,
	'created_at': '2021-08-27 20:48:10.962 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6119b51e5916290067b823cd',
	'name': 'Luke Mckechnie',
	'healthScore': 1.3079748768220167,
	'created_at': '2021-08-16 00:45:18.327 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6119b4855916290069a92200',
	'name': 'Alejandro Munguia',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-08-16 00:42:45.087 +0000',
	'deleted_at': '2021-08-30 14:08:15.898 +0000',
	'daysEmployed': 14,
	'daysLeft': 402
}, {
	'user_id': '6119b4855916290066ccc34c',
	'name': 'Andrea Stutesman',
	'healthScore': 1.5809498207328927,
	'created_at': '2021-08-16 00:42:45.687 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6119b4355916290067b823ab',
	'name': 'Mollie Hinz',
	'healthScore': 2.1158582808900706,
	'created_at': '2021-08-16 00:41:25.735 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '6119b4355916290066ccc344',
	'name': 'Rachael Maruca',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-08-16 00:41:26.232 +0000',
	'deleted_at': '2022-07-15 22:07:27.551 +0000',
	'daysEmployed': 333,
	'daysLeft': 83
}, {
	'user_id': '6119b3ef5916290066ccc33e',
	'name': 'Namaste Reid',
	'healthScore': 2.882886475220195,
	'created_at': '2021-08-16 00:40:17.030 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61073ed8d26743006ad94db7',
	'name': 'Oliver De La Via',
	'healthScore': 2.073783420737226,
	'created_at': '2021-08-02 00:39:52.299 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61073ebbd26743006ad94db0',
	'name': 'William Rudnick',
	'healthScore': 2.5111989172459004,
	'created_at': '2021-08-02 00:39:23.412 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '61073eb9d2674300691bec99',
	'name': 'Henry Duong',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-08-02 00:39:22.004 +0000',
	'deleted_at': '2022-07-09 01:56:01.981 +0000',
	'daysEmployed': 341,
	'daysLeft': 89
}, {
	'user_id': '60f74b8f38f86f0067700945',
	'name': 'Laura Saracho',
	'healthScore': 1.9879544643168199,
	'created_at': '2021-07-20 22:17:51.205 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60f4ca36983d500069cc7057',
	'name': 'Amanda Herschleb',
	'healthScore': 1.4280996808519792,
	'created_at': '2021-07-19 00:41:26.601 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60f4ca01983d500067e62c36',
	'name': 'Gregory Walker',
	'healthScore': 1.2671731779377637,
	'created_at': '2021-07-19 00:40:33.460 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60f4c9dc983d5000683716d7',
	'name': 'Daniel Castillo Oropeza',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-07-19 00:39:56.649 +0000',
	'deleted_at': '2022-07-14 15:35:20.966 +0000',
	'daysEmployed': 360,
	'daysLeft': 84
}, {
	'user_id': '60f4c9a4983d500067e62c23',
	'name': 'Timothy Hodges',
	'healthScore': 0.5667475534083654,
	'created_at': '2021-07-19 00:39:00.218 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60f4c980983d5000683716ce',
	'name': 'Kailene Robinson',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-07-19 00:38:24.261 +0000',
	'deleted_at': '2022-05-28 07:17:08.393 +0000',
	'daysEmployed': 313,
	'daysLeft': 131
}, {
	'user_id': '60f1d1631dda8c0068e9737e',
	'name': 'Lillian Gately',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-07-16 18:35:15.688 +0000',
	'deleted_at': '2022-07-14 15:35:21.170 +0000',
	'daysEmployed': 363,
	'daysLeft': 84
}, {
	'user_id': '60edcb5c9a833d00d8060a0f',
	'name': 'Kim Williams',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-07-13 17:20:29.018 +0000',
	'deleted_at': '2021-07-13 17:23:16.814 +0000',
	'daysEmployed': 0,
	'daysLeft': 450
}, {
	'user_id': '60e84db40ba959006909889b',
	'name': 'Tore Anderson',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-07-09 13:23:00.969 +0000',
	'deleted_at': '2021-09-28 07:12:39.272 +0000',
	'daysEmployed': 81,
	'daysLeft': 373
}, {
	'user_id': '60e3a6d71a6fe300698a6326',
	'name': 'Keith Edward',
	'healthScore': 0.7120351449714131,
	'created_at': '2021-07-06 00:41:59.969 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60cfe035504f590068c54cd3',
	'name': 'Jefferey Sutherland',
	'healthScore': 0.7372222118734431,
	'created_at': '2021-06-21 00:41:25.207 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60cfe012504f5900673e5224',
	'name': 'Vanessa Kahn',
	'healthScore': 0.6734951777157252,
	'created_at': '2021-06-21 00:40:50.866 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60cfe012504f5900673e5221',
	'name': 'Robert Vincent',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-06-21 00:40:50.841 +0000',
	'deleted_at': '2022-05-28 07:13:25.265 +0000',
	'daysEmployed': 341,
	'daysLeft': 131
}, {
	'user_id': '60cfe012504f5900673e5220',
	'name': 'Sara Meier',
	'healthScore': 1.0226386554244633,
	'created_at': '2021-06-21 00:40:50.828 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60cfe001504f59006977e931',
	'name': 'Jared Cassidy',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-06-21 00:40:33.859 +0000',
	'deleted_at': '2022-05-28 07:17:41.318 +0000',
	'daysEmployed': 341,
	'daysLeft': 131
}, {
	'user_id': '60cfdf9f504f59006977e92a',
	'name': 'Spencer Hurley',
	'healthScore': 0.9460762740775115,
	'created_at': '2021-06-21 00:38:55.084 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60cfdf78504f590066159f8a',
	'name': 'Bonnie Demuth',
	'healthScore': 0.9721302479147704,
	'created_at': '2021-06-21 00:38:16.258 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60aaf60b6a6245006aa53e9b',
	'name': 'Logan Salat',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-24 00:40:43.620 +0000',
	'deleted_at': '2022-06-02 07:13:25.863 +0000',
	'daysEmployed': 374,
	'daysLeft': 126
}, {
	'user_id': '60aaf5d76a6245006aa53e92',
	'name': 'Philip Lipman',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-24 00:39:51.636 +0000',
	'deleted_at': '2022-04-02 07:17:26.277 +0000',
	'daysEmployed': 313,
	'daysLeft': 187
}, {
	'user_id': '609c3ef50e5a650012ab492d',
	'name': 'Taylor Johnson',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-12 20:47:49.602 +0000',
	'deleted_at': '2022-06-11 07:25:50.529 +0000',
	'daysEmployed': 395,
	'daysLeft': 117
}, {
	'user_id': '609881b8eed78c00148f4086',
	'name': 'Andrew Pabon',
	'healthScore': 1.2536152091619357,
	'created_at': '2021-05-10 00:43:37.130 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '609880c9eed78c0011749595',
	'name': 'Adam Kirschbaum',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-10 00:39:37.820 +0000',
	'deleted_at': '2022-02-03 22:00:17.661 +0000',
	'daysEmployed': 269,
	'daysLeft': 245
}, {
	'user_id': '60901c131277040014a45203',
	'name': 'Dylan Reno',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-03 15:51:47.586 +0000',
	'deleted_at': '2021-10-21 07:12:26.781 +0000',
	'daysEmployed': 171,
	'daysLeft': 350
}, {
	'user_id': '608f465a4b77260014b36465',
	'name': 'Kaitlyn Gold',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-05-03 00:39:54.311 +0000',
	'deleted_at': '2021-08-03 07:15:07.408 +0000',
	'daysEmployed': 92,
	'daysLeft': 429
}, {
	'user_id': '608f46594b77260012dde054',
	'name': 'Chynna Delgado',
	'healthScore': 1.6025367160278423,
	'created_at': '2021-05-03 00:39:53.461 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '608f46374b77260013095c93',
	'name': 'Steven McNiff',
	'healthScore': 0.7133404485463246,
	'created_at': '2021-05-03 00:39:19.906 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60860bfdb380bf0011b08237',
	'name': 'Shawn Azar',
	'healthScore': 0.7774961239261106,
	'created_at': '2021-04-26 00:40:29.700 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '607cd2bb095b6d0013c7d667',
	'name': 'Daniel Hubbard',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-04-19 00:45:47.751 +0000',
	'deleted_at': '2022-03-02 01:28:34.542 +0000',
	'daysEmployed': 317,
	'daysLeft': 218
}, {
	'user_id': '607cd123095b6d00125c4c3d',
	'name': 'Karin Anderson',
	'healthScore': 1.0557021418292913,
	'created_at': '2021-04-19 00:38:59.721 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60739810f4f6ba00127f6764',
	'name': 'Jonathan Nguyen',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-04-12 00:45:04.773 +0000',
	'deleted_at': '2022-04-02 07:13:01.243 +0000',
	'daysEmployed': 355,
	'daysLeft': 187
}, {
	'user_id': '605a89eee6f8ee001463f5ff',
	'name': 'Kelsey Kinkead',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-03-24 00:38:06.882 +0000',
	'deleted_at': '2021-08-07 07:12:53.881 +0000',
	'daysEmployed': 136,
	'daysLeft': 425
}, {
	'user_id': '6058d4a0d51ffc00140c2ba8',
	'name': 'Matthew Perry',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-03-22 17:32:16.300 +0000',
	'deleted_at': '2022-03-04 18:25:07.063 +0000',
	'daysEmployed': 347,
	'daysLeft': 216
}, {
	'user_id': '603c45ff70c54d00145cf2bd',
	'name': 'Brad Matsushita',
	'healthScore': 0.8660012582193405,
	'created_at': '2021-03-01 01:40:15.430 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60330c2561f9fe00121f1dbd',
	'name': 'Herman Sullivan',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-02-22 01:43:01.245 +0000',
	'deleted_at': '2022-04-26 07:19:20.581 +0000',
	'daysEmployed': 428,
	'daysLeft': 163
}, {
	'user_id': '60330bcf61f9fe0014713247',
	'name': 'Jay Soneff Jr.',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-02-22 01:41:35.049 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60330b2c61f9fe0011b16ee3',
	'name': 'Brian Noyle',
	'healthScore': 0.7021388842949303,
	'created_at': '2021-02-22 01:38:52.222 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '60330b0d61f9fe0011b16eda',
	'name': 'Victoria Yang',
	'healthScore': 3.6047240073376603,
	'created_at': '2021-02-22 01:38:21.624 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '602f1381e78df000b33a7a84',
	'name': 'Fabiola Delgado',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-02-19 01:25:21.104 +0000',
	'deleted_at': '2021-02-19 01:27:16.398 +0000',
	'daysEmployed': 0,
	'daysLeft': 594
}, {
	'user_id': '5ffbae05ed4e82001218b5f5',
	'name': 'Beatriz Bours',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-01-11 01:46:45.214 +0000',
	'deleted_at': '2022-04-26 07:20:50.743 +0000',
	'daysEmployed': 470,
	'daysLeft': 163
}, {
	'user_id': '5ffbad9bed4e8200139e3b97',
	'name': 'Jack Mayfield',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-01-11 01:44:59.838 +0000',
	'deleted_at': '2021-12-02 08:16:49.038 +0000',
	'daysEmployed': 325,
	'daysLeft': 308
}, {
	'user_id': '5ffbac8eed4e8200139e3b8b',
	'name': 'Hannah Treman',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-01-11 01:40:30.905 +0000',
	'deleted_at': '2022-03-02 01:30:28.979 +0000',
	'daysEmployed': 415,
	'daysLeft': 218
}, {
	'user_id': '5ffbac1ced4e820015bacc50',
	'name': 'erica wilhelmy',
	'healthScore': 0.15000000000000002,
	'created_at': '2021-01-11 01:38:36.261 +0000',
	'deleted_at': '2021-04-06 07:11:50.226 +0000',
	'daysEmployed': 85,
	'daysLeft': 548
}, {
	'user_id': '5ff27273a00a4a0014d1abe1',
	'name': 'Desiree Mitcham',
	'healthScore': 0.36710672468431904,
	'created_at': '2021-01-04 01:42:11.685 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5fd6c50458b0e3001250273e',
	'name': 'James Foreman',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-12-14 01:51:00.099 +0000',
	'deleted_at': '2022-03-02 01:28:07.657 +0000',
	'daysEmployed': 443,
	'daysLeft': 218
}, {
	'user_id': '5fd6c50258b0e30014f4637e',
	'name': 'Mia Medico',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-12-14 01:50:58.999 +0000',
	'deleted_at': '2021-07-10 07:13:28.662 +0000',
	'daysEmployed': 208,
	'daysLeft': 453
}, {
	'user_id': '5fd6c4e358b0e30012502733',
	'name': 'Viet Le',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-12-14 01:50:27.248 +0000',
	'deleted_at': '2021-11-02 07:14:39.844 +0000',
	'daysEmployed': 323,
	'daysLeft': 338
}, {
	'user_id': '5fd6c4e358b0e30012502732',
	'name': 'Shaheen Axtle',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-12-14 01:50:27.242 +0000',
	'deleted_at': '2021-10-16 07:14:34.980 +0000',
	'daysEmployed': 306,
	'daysLeft': 355
}, {
	'user_id': '5fd6c44258b0e30011a6c7e6',
	'name': 'Brian McKinney',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-12-14 01:47:46.497 +0000',
	'deleted_at': '2021-06-26 07:14:05.485 +0000',
	'daysEmployed': 194,
	'daysLeft': 467
}, {
	'user_id': '5fb5ce2eaef07900136b372b',
	'name': 'Brant Wellman',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-11-19 01:45:18.512 +0000',
	'deleted_at': '2021-10-20 07:17:05.922 +0000',
	'daysEmployed': 335,
	'daysLeft': 351
}, {
	'user_id': '5fa9a78b4f4e7a0015627a17',
	'name': 'Alexander Robertson',
	'healthScore': 0.31481583376073874,
	'created_at': '2020-11-09 20:33:15.731 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5f90ae098bd8650054002b35',
	'name': 'Liza Lebovitz',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-10-21 21:54:17.366 +0000',
	'deleted_at': '2022-01-12 00:36:23.828 +0000',
	'daysEmployed': 448,
	'daysLeft': 267
}, {
	'user_id': '5f7b842decc7b4001326332b',
	'name': 'Erin Hinson',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-10-05 20:38:05.167 +0000',
	'deleted_at': '2022-04-02 07:11:38.151 +0000',
	'daysEmployed': 544,
	'daysLeft': 187
}, {
	'user_id': '5f51701bdbc0d600c15ea56f',
	'name': 'William Kearney',
	'healthScore': 0.4507180022415066,
	'created_at': '2020-09-03 22:37:15.214 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5f4cf8fb02a286001320d57f',
	'name': 'Riegel Brasseux',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-08-31 13:19:55.616 +0000',
	'deleted_at': '2022-02-04 22:03:25.054 +0000',
	'daysEmployed': 522,
	'daysLeft': 244
}, {
	'user_id': '5edddccb61e6a7001489013b',
	'name': 'Helen Murphy',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-06-08 06:38:03.175 +0000',
	'deleted_at': '2022-04-09 07:15:08.940 +0000',
	'daysEmployed': 670,
	'daysLeft': 180
}, {
	'user_id': '5e9d51418334d200151c6f87',
	'name': 'Richard Lansky',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-04-20 07:37:37.477 +0000',
	'deleted_at': '2020-08-06 07:12:43.370 +0000',
	'daysEmployed': 108,
	'daysLeft': 791
}, {
	'user_id': '5e8adc8fedf8c2001500f3dc',
	'name': 'Sheala Lennox',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-04-06 07:38:55.781 +0000',
	'deleted_at': '2021-04-06 07:11:50.892 +0000',
	'daysEmployed': 365,
	'daysLeft': 548
}, {
	'user_id': '5e8adc8fedf8c200146a77f7',
	'name': 'Valerie Zolyak',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-04-06 07:38:55.815 +0000',
	'deleted_at': '2022-02-03 21:55:35.298 +0000',
	'daysEmployed': 668,
	'daysLeft': 245
}, {
	'user_id': '5e7b0a37c66ce30014387f9b',
	'name': 'Christopher Gamble',
	'healthScore': 1.174218109267238,
	'created_at': '2020-03-25 07:37:27.589 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5e6614393c8e3800140cb5ad',
	'name': 'Adrianne Hipsher',
	'healthScore': 1.8832597370634345,
	'created_at': '2020-03-09 10:02:33.547 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5e65f47c3c8e380013d01e9a',
	'name': 'Jennifer Perocchi',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-03-09 07:47:08.390 +0000',
	'deleted_at': '2021-08-27 15:48:34.103 +0000',
	'daysEmployed': 536,
	'daysLeft': 405
}, {
	'user_id': '5e65f4613c8e3800124578a5',
	'name': 'Michael Mocerino',
	'healthScore': 0.4632667699843668,
	'created_at': '2020-03-09 07:46:41.528 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5e65f4213c8e380013d01e81',
	'name': 'Nowai Matthew',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-03-09 07:45:37.465 +0000',
	'deleted_at': '2021-09-07 13:56:18.979 +0000',
	'daysEmployed': 547,
	'daysLeft': 394
}, {
	'user_id': '5e3af39d0e8b05001359bf51',
	'name': 'Katharine Cygan',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-02-05 16:55:57.037 +0000',
	'deleted_at': '2021-10-30 07:11:56.230 +0000',
	'daysEmployed': 633,
	'daysLeft': 341
}, {
	'user_id': '5e3ae9a00e8b0500149838f8',
	'name': 'Savannah Waggoner',
	'healthScore': 0.15000000000000002,
	'created_at': '2020-02-05 16:13:20.457 +0000',
	'deleted_at': '2022-01-06 22:35:06.903 +0000',
	'daysEmployed': 701,
	'daysLeft': 273
}, {
	'user_id': '5e12f2d8938ac5001332563f',
	'name': 'Traci Thompson',
	'healthScore': 0.47906714260650346,
	'created_at': '2020-01-06 08:42:00.230 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5dbfe36a6766e40042a548e1',
	'name': 'Stacy Tumarkin',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-11-04 08:38:02.159 +0000',
	'deleted_at': '2020-06-24 07:11:22.128 +0000',
	'daysEmployed': 233,
	'daysLeft': 834
}, {
	'user_id': '5d91b16010f0580043da2c8b',
	'name': 'Katherine Nihen',
	'healthScore': 0.9609848970495312,
	'created_at': '2019-09-30 07:40:16.231 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5d7f3da605ad94004352ca64',
	'name': 'Ford Knowlton',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-09-16 07:45:42.200 +0000',
	'deleted_at': '2020-09-10 07:12:34.690 +0000',
	'daysEmployed': 360,
	'daysLeft': 756
}, {
	'user_id': '5d7f3bfd05ad94004192f9f6',
	'name': 'Carlos Santana',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-09-16 07:38:37.187 +0000',
	'deleted_at': '2022-02-03 21:51:34.517 +0000',
	'daysEmployed': 871,
	'daysLeft': 245
}, {
	'user_id': '5d7edb6083aeff0044cbfa97',
	'name': 'Bill Munroe',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-09-16 00:46:25.430 +0000',
	'deleted_at': '2020-11-09 15:48:44.473 +0000',
	'daysEmployed': 420,
	'daysLeft': 696
}, {
	'user_id': '5d766d8084ef020041389846',
	'name': 'Albert Wavering',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-09-09 15:19:28.429 +0000',
	'deleted_at': '2022-03-02 01:26:51.332 +0000',
	'daysEmployed': 905,
	'daysLeft': 218
}, {
	'user_id': '5d760191b0f2eb0040833ccc',
	'name': 'Nicholas Watson',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-09-09 07:38:57.486 +0000',
	'deleted_at': '2021-01-29 08:11:48.857 +0000',
	'daysEmployed': 508,
	'daysLeft': 615
}, {
	'user_id': '5d5a52eee5fc790040bd9a1e',
	'name': 'Steven Demchuk',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-19 07:42:38.164 +0000',
	'deleted_at': '2022-01-06 22:31:54.683 +0000',
	'daysEmployed': 871,
	'daysLeft': 273
}, {
	'user_id': '5d5a52bae5fc79004102148a',
	'name': 'Caroline Jenkins',
	'healthScore': 2.383987501158133,
	'created_at': '2019-08-19 07:41:46.454 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5d431f6fc2515b000b8bcaa8',
	'name': 'Permissive Pat Bot',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-01 17:20:50.228 +0000',
	'deleted_at': '2019-11-05 17:43:59.196 +0000',
	'daysEmployed': 96,
	'daysLeft': 1066
}, {
	'user_id': '5d431f4cc2515b000b8bcaa2',
	'name': 'Rewards Randi Bot',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-01 17:20:15.549 +0000',
	'deleted_at': '2019-11-05 17:44:14.258 +0000',
	'daysEmployed': 96,
	'daysLeft': 1066
}, {
	'user_id': '5d431f26c2515b000b8bca9c',
	'name': 'Tech Terri Bot',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-01 17:19:35.909 +0000',
	'deleted_at': '2019-11-05 17:44:23.077 +0000',
	'daysEmployed': 96,
	'daysLeft': 1066
}, {
	'user_id': '5d431ecac2515b000b8bca95',
	'name': 'Finance Freddy Bot',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-01 17:18:05.992 +0000',
	'deleted_at': '2019-11-05 17:44:06.043 +0000',
	'daysEmployed': 96,
	'daysLeft': 1066
}, {
	'user_id': '5d4296ccc9e6370041e53d3d',
	'name': 'Elias Julian',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-08-01 07:37:48.829 +0000',
	'deleted_at': '2022-03-02 01:30:52.853 +0000',
	'daysEmployed': 944,
	'daysLeft': 218
}, {
	'user_id': '5d2838b736702200419a4abd',
	'name': 'Bryan Thompson',
	'healthScore': 0.48079889919862373,
	'created_at': '2019-07-12 07:37:27.425 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5d22f2ddca90c20040a19222',
	'name': 'Maxim Pekarsky',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-07-08 07:38:05.151 +0000',
	'deleted_at': '2021-07-02 07:14:09.053 +0000',
	'daysEmployed': 725,
	'daysLeft': 461
}, {
	'user_id': '5d19b87253df590041533703',
	'name': 'Ambica Avancha',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-07-01 07:38:26.556 +0000',
	'deleted_at': '2021-07-23 07:13:47.854 +0000',
	'daysEmployed': 753,
	'daysLeft': 440
}, {
	'user_id': '5cf4cf2575a273002e53ff3a',
	'name': 'Ellen Cornelius',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-06-03 07:41:25.571 +0000',
	'deleted_at': '2021-01-12 21:10:34.012 +0000',
	'daysEmployed': 589,
	'daysLeft': 632
}, {
	'user_id': '5cece54c86fc4d002fb262f7',
	'name': 'Andrew Jaswa',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-05-28 07:37:48.183 +0000',
	'deleted_at': '2021-09-13 17:02:25.021 +0000',
	'daysEmployed': 839,
	'daysLeft': 388
}, {
	'user_id': '5cd3d8abba4656002efddb77',
	'name': 'Thomas Williams',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-05-09 07:37:15.546 +0000',
	'deleted_at': '2021-03-27 07:11:32.820 +0000',
	'daysEmployed': 688,
	'daysLeft': 558
}, {
	'user_id': '5cd3d8a7ba4656002fe12765',
	'name': 'Sungwoo Chris Park',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-05-09 07:37:11.232 +0000',
	'deleted_at': '2021-02-06 08:11:03.192 +0000',
	'daysEmployed': 639,
	'daysLeft': 607
}, {
	'user_id': '5cb434f9a40b7c002d4f4f4b',
	'name': 'Tian Chuan Yen',
	'healthScore': 1.9073314044419158,
	'created_at': '2019-04-15 07:38:33.344 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5c992ec23b2076000b2f0f1d',
	'name': 'Bonusly Dev',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-03-25 19:40:54.967 +0000',
	'deleted_at': '2019-03-25 20:02:10.032 +0000',
	'daysEmployed': 0,
	'daysLeft': 1291
}, {
	'user_id': '5c98856da99fa5002fdf9b29',
	'name': 'Connie Du',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-03-25 07:38:21.116 +0000',
	'deleted_at': '2022-02-03 21:49:03.979 +0000',
	'daysEmployed': 1046,
	'daysLeft': 245
}, {
	'user_id': '5c6d11db7da297002f10b2fa',
	'name': 'Katherine Farrar',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-02-20 08:37:47.116 +0000',
	'deleted_at': '2021-01-12 21:10:32.131 +0000',
	'daysEmployed': 692,
	'daysLeft': 632
}, {
	'user_id': '5c47402f8cf435002fef17e8',
	'name': 'Lauren Risberg',
	'healthScore': 0.15000000000000002,
	'created_at': '2019-01-22 16:09:19.308 +0000',
	'deleted_at': '2021-06-17 07:11:44.029 +0000',
	'daysEmployed': 877,
	'daysLeft': 476
}, {
	'user_id': '5b9f5a0bbda25b000d60cf9f',
	'name': 'Brian Johnson',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-09-17 07:38:51.772 +0000',
	'deleted_at': '2021-01-30 08:12:18.271 +0000',
	'daysEmployed': 866,
	'daysLeft': 614
}, {
	'user_id': '5b71354a908b7a000d59872f',
	'name': 'Christopher Chun',
	'healthScore': 0.38481435115764123,
	'created_at': '2018-08-13 07:37:49.031 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '5b71352f388909000dbc517d',
	'name': 'Caroline Clark',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-08-13 07:37:19.880 +0000',
	'deleted_at': '2021-01-12 21:10:30.959 +0000',
	'daysEmployed': 883,
	'daysLeft': 632
}, {
	'user_id': '5b27616aa2eb42280a702a89',
	'name': 'Caleb Barde',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-06-18 07:38:18.845 +0000',
	'deleted_at': '2021-01-12 21:10:30.693 +0000',
	'daysEmployed': 939,
	'daysLeft': 632
}, {
	'user_id': '5b0e54a8c0220508292ed1f2',
	'name': 'Benjamin Travis',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-05-30 07:37:12.912 +0000',
	'deleted_at': '2021-06-24 07:10:58.572 +0000',
	'daysEmployed': 1121,
	'daysLeft': 469
}, {
	'user_id': '5b027746f623ca1ad24ecbda',
	'name': 'Allan McLelland',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-05-21 07:37:42.833 +0000',
	'deleted_at': '2021-01-12 21:10:30.391 +0000',
	'daysEmployed': 967,
	'daysLeft': 632
}, {
	'user_id': '5a990d35b33b1a27e43b1ee3',
	'name': 'Danielle Shoback',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-03-02 08:37:09.943 +0000',
	'deleted_at': '2022-01-07 17:44:54.366 +0000',
	'daysEmployed': 1407,
	'daysLeft': 272
}, {
	'user_id': '5a7818378d4858279f17334c',
	'name': 'Amber Redig',
	'healthScore': 0.15000000000000002,
	'created_at': '2018-02-05 08:39:19.541 +0000',
	'deleted_at': '2022-05-21 07:15:10.056 +0000',
	'daysEmployed': 1566,
	'daysLeft': 138
}, {
	'user_id': '5a4677f2a56d9d394d8d9b75',
	'name': 'John Doe',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-12-29 17:14:26.603 +0000',
	'deleted_at': '2021-01-12 21:10:29.446 +0000',
	'daysEmployed': 1110,
	'daysLeft': 632
}, {
	'user_id': '5a2509608e8e96014e9bd9e2',
	'name': 'Marcus Banks',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-12-04 08:37:52.281 +0000',
	'deleted_at': '2021-01-12 21:10:29.165 +0000',
	'daysEmployed': 1135,
	'daysLeft': 632
}, {
	'user_id': '59ae5438555fc84168639a4a',
	'name': 'Armando Leon',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-09-05 07:37:29.220 +0000',
	'deleted_at': '2021-08-03 07:14:39.291 +0000',
	'daysEmployed': 1428,
	'daysLeft': 429
}, {
	'user_id': '59aa2c878430796d67f27e1f',
	'name': 'Elisse Lockhart',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-09-02 03:59:04.042 +0000',
	'deleted_at': '2022-01-20 15:31:29.049 +0000',
	'daysEmployed': 1601,
	'daysLeft': 259
}, {
	'user_id': '599d30a4b3234d4a6f129498',
	'name': 'Joao Filipe Campos',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-08-23 07:37:09.268 +0000',
	'deleted_at': '2022-07-15 22:28:39.733 +0000',
	'daysEmployed': 1787,
	'daysLeft': 83
}, {
	'user_id': '597846a7e8d4ff5b96d851da',
	'name': 'Mark Peck',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-07-26 07:37:12.272 +0000',
	'deleted_at': '2021-01-12 21:10:27.903 +0000',
	'daysEmployed': 1266,
	'daysLeft': 632
}, {
	'user_id': '59477f745d51ff194bc39c4e',
	'name': 'Maja Orsic',
	'healthScore': 0.625988748611269,
	'created_at': '2017-06-19 07:38:28.779 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '59350a4af9f4ff084f7a0df6',
	'name': 'Daniel Murphy',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-06-05 07:37:47.144 +0000',
	'deleted_at': '2021-02-09 08:11:48.619 +0000',
	'daysEmployed': 1345,
	'daysLeft': 604
}, {
	'user_id': '5919f0442d6c5d10d34f14f7',
	'name': 'Anna Dolan',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-05-15 18:15:32.834 +0000',
	'deleted_at': '2021-06-02 07:11:12.888 +0000',
	'daysEmployed': 1479,
	'daysLeft': 491
}, {
	'user_id': '5905943df22bed0443ce9ec7',
	'name': 'Jessica Nerad',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-04-30 07:37:33.680 +0000',
	'deleted_at': '2021-01-12 21:10:26.614 +0000',
	'daysEmployed': 1353,
	'daysLeft': 632
}, {
	'user_id': '59004e2aee9566592b0e0609',
	'name': 'Madison Beckmann',
	'healthScore': 0.15000000000000002,
	'created_at': '2017-04-26 07:37:14.502 +0000',
	'deleted_at': '2021-11-13 08:15:53.993 +0000',
	'daysEmployed': 1662,
	'daysLeft': 327
}, {
	'user_id': '5832b47622a5684a1b330007',
	'name': 'Josh Cass',
	'healthScore': 0.9625839886493333,
	'created_at': '2016-11-21 08:46:47.555 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}, {
	'user_id': '57f6c3756377710371e02f15',
	'name': 'Dani Black',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-10-06 21:34:46.140 +0000',
	'deleted_at': '2021-01-12 21:10:25.719 +0000',
	'daysEmployed': 1559,
	'daysLeft': 632
}, {
	'user_id': '57963798fd93af0289178205',
	'name': 'Jonathan Evans',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-07-25 16:00:24.454 +0000',
	'deleted_at': '2021-05-13 07:11:40.421 +0000',
	'daysEmployed': 1753,
	'daysLeft': 511
}, {
	'user_id': '57589840a245bc94c6000022',
	'name': 'Robert Ingrum',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-06-08 22:12:16.822 +0000',
	'deleted_at': '2021-01-12 21:10:24.166 +0000',
	'daysEmployed': 1679,
	'daysLeft': 632
}, {
	'user_id': '574375afe19d4f921b000065',
	'name': 'Derrick Parkhurst',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-05-23 21:27:11.350 +0000',
	'deleted_at': '2016-05-25 01:45:10.126 +0000',
	'daysEmployed': 2,
	'daysLeft': 2325
}, {
	'user_id': '56fd5ade743d0f671500005e',
	'name': 'Emily Ciavolino',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-03-31 17:14:06.757 +0000',
	'deleted_at': '2021-01-12 21:10:23.885 +0000',
	'daysEmployed': 1748,
	'daysLeft': 632
}, {
	'user_id': '56b8dedfb92bb2fd5d000017',
	'name': 'Aaron Test',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-02-08 18:30:55.403 +0000',
	'deleted_at': '2016-02-09 22:53:44.744 +0000',
	'daysEmployed': 1,
	'daysLeft': 2431
}, {
	'user_id': '56b8d18d953398bc4b000002',
	'name': 'Aaron Test',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-02-08 17:34:05.572 +0000',
	'deleted_at': '2016-02-08 18:30:25.392 +0000',
	'daysEmployed': 0,
	'daysLeft': 2432
}, {
	'user_id': '568d3c1a1e2357524b000001',
	'name': 'boulder wormhole',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-01-06 16:08:58.005 +0000',
	'deleted_at': '2016-01-19 18:32:25.387 +0000',
	'daysEmployed': 13,
	'daysLeft': 2452
}, {
	'user_id': '568d3c189520b12ad0000001',
	'name': 'brooklyn wormhole',
	'healthScore': 0.15000000000000002,
	'created_at': '2016-01-06 16:08:56.420 +0000',
	'deleted_at': '2016-01-19 18:32:25.387 +0000',
	'daysEmployed': 13,
	'daysLeft': 2452
}, {
	'user_id': '5669a7fe5e516cc029000001',
	'name': 'Testerini Demo',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-10 16:27:42.555 +0000',
	'deleted_at': '2015-12-17 16:44:42.506 +0000',
	'daysEmployed': 7,
	'daysLeft': 2485
}, {
	'user_id': '56673405b4bf68dfe2000001',
	'name': 'Testerinoo1 Demoo1',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-08 19:48:21.218 +0000',
	'deleted_at': '2015-12-13 00:03:20.339 +0000',
	'daysEmployed': 5,
	'daysLeft': 2489
}, {
	'user_id': '56673320f6374526d0000001',
	'name': 'testerino demographics',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-08 19:44:32.045 +0000',
	'deleted_at': '2015-12-08 21:19:08.906 +0000',
	'daysEmployed': 0,
	'daysLeft': 2494
}, {
	'user_id': '5667314da0e448d6c1000001',
	'name': 'Testy Mctesterson',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-08 19:36:45.675 +0000',
	'deleted_at': '2015-12-08 21:19:17.586 +0000',
	'daysEmployed': 0,
	'daysLeft': 2494
}, {
	'user_id': '5665b737efd5901353000001',
	'name': 'No Reply',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-07 16:43:35.195 +0000',
	'deleted_at': '2016-01-19 18:33:09.052 +0000',
	'daysEmployed': 43,
	'daysLeft': 2452
}, {
	'user_id': '5665b7375a203fa93b000001',
	'name': 'Android Development',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-07 16:43:35.130 +0000',
	'deleted_at': '2015-12-10 17:58:11.144 +0000',
	'daysEmployed': 3,
	'daysLeft': 2492
}, {
	'user_id': '5665b736a96b81ca96000001',
	'name': 'admin user',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-07 16:43:34.667 +0000',
	'deleted_at': '2016-01-19 18:32:25.387 +0000',
	'daysEmployed': 43,
	'daysLeft': 2452
}, {
	'user_id': '5665b7365a203f4a69000001',
	'name': 'Aaron Davis',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-12-07 16:43:34.313 +0000',
	'deleted_at': '2021-01-12 21:10:23.032 +0000',
	'daysEmployed': 1863,
	'daysLeft': 632
}, {
	'user_id': '565cb367b769e8ae9a000008',
	'name': 'Elizabeth Ernst',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-11-30 20:36:55.689 +0000',
	'deleted_at': '2022-05-21 07:12:54.570 +0000',
	'daysEmployed': 2364,
	'daysLeft': 138
}, {
	'user_id': '564507ba998a1b72a4000021',
	'name': 'Test User',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-11-12 21:42:18.223 +0000',
	'deleted_at': '2015-11-23 18:01:49.395 +0000',
	'daysEmployed': 11,
	'daysLeft': 2509
}, {
	'user_id': '5637b72513e09f742c00000a',
	'name': 'Andrew Brown',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-11-02 19:19:01.321 +0000',
	'deleted_at': '2016-07-15 16:12:26.787 +0000',
	'daysEmployed': 256,
	'daysLeft': 2274
}, {
	'user_id': '562940f441f6cd10fe000030',
	'name': 'Todd Resudek',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-10-22 20:03:00.103 +0000',
	'deleted_at': '2016-05-12 13:45:49.956 +0000',
	'daysEmployed': 203,
	'daysLeft': 2338
}, {
	'user_id': '55f1a1b3f194450c15000041',
	'name': 'Aron Greenspan',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-09-10 15:28:51.551 +0000',
	'deleted_at': '2016-02-29 21:57:33.998 +0000',
	'daysEmployed': 172,
	'daysLeft': 2411
}, {
	'user_id': '55e8b5ab4f182f529f000015',
	'name': 'sdlkfjs lskjdf',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-09-03 21:03:39.530 +0000',
	'deleted_at': '2015-09-03 21:03:52.431 +0000',
	'daysEmployed': 0,
	'daysLeft': 2590
}, {
	'user_id': '55e736072f97dbf8a5000017',
	'name': 'Carlton Bowers',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-09-02 17:46:47.927 +0000',
	'deleted_at': '2021-01-12 21:10:23.588 +0000',
	'daysEmployed': 1959,
	'daysLeft': 632
}, {
	'user_id': '55a803e3ddf13a099b000003',
	'name': 'Kuldar Kalvik',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-07-16 19:20:03.255 +0000',
	'deleted_at': '2016-07-15 16:12:19.233 +0000',
	'daysEmployed': 365,
	'daysLeft': 2274
}, {
	'user_id': '5575b6063cac5dfe25000010',
	'name': 'Tyler Heck',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-06-08 15:34:30.056 +0000',
	'deleted_at': '2016-02-16 01:40:29.507 +0000',
	'daysEmployed': 253,
	'daysLeft': 2424
}, {
	'user_id': '5567d020ec88564f9e000005',
	'name': 'Sahil Mehta',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-05-29 02:34:08.423 +0000',
	'deleted_at': '2021-01-12 21:10:24.454 +0000',
	'daysEmployed': 2055,
	'daysLeft': 632
}, {
	'user_id': '551e96c104612414f700002e',
	'name': ' ',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-04-03 13:33:53.711 +0000',
	'deleted_at': '2014-04-03 13:33:56.000 +0000',
	'daysEmployed': -365,
	'daysLeft': 3108
}, {
	'user_id': '55198c7a433720021a00004e',
	'name': 'Todd Zusman',
	'healthScore': 0.15000000000000002,
	'created_at': '2015-03-30 17:48:42.746 +0000',
	'deleted_at': '2016-01-19 18:31:02.509 +0000',
	'daysEmployed': 295,
	'daysLeft': 2452
}, {
	'user_id': '5429c40125d5e0c04f000001',
	'name': 'George Dickson',
	'healthScore': 0.15000000000000002,
	'created_at': '2014-09-29 20:41:37.407 +0000',
	'deleted_at': '2021-01-12 21:10:22.733 +0000',
	'daysEmployed': 2297,
	'daysLeft': 632
}, {
	'user_id': '53445c771222fd478f000001',
	'name': 'Gavin Strumpman',
	'healthScore': 0.15000000000000002,
	'created_at': '2014-04-08 20:30:47.141 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': -6,
	'daysLeft': 3109
}, {
	'user_id': '534050b64432b70bb5000001',
	'name': 'Biz Ghormley',
	'healthScore': 0.15000000000000002,
	'created_at': '2014-04-05 18:51:34.171 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': -3,
	'daysLeft': 3109
}, {
	'user_id': '52eda094d580fd8fd7000007',
	'name': 'Swingline Waddams',
	'healthScore': 0.15000000000000002,
	'created_at': '2014-02-02 01:34:12.558 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': 59,
	'daysLeft': 3109
}, {
	'user_id': '51f95e7375df08887b000001',
	'name': 'Chris Gale',
	'healthScore': 0.15000000000000002,
	'created_at': '2013-07-31 18:58:59.933 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': 245,
	'daysLeft': 3109
}, {
	'user_id': '51339ecb45bb2500c8000008',
	'name': 'Traunza Adams',
	'healthScore': 0.15000000000000002,
	'created_at': '2013-03-03 19:04:43.208 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': 395,
	'daysLeft': 3109
}, {
	'user_id': '50aa452af667111519000024',
	'name': 'alshgdla laksdglah',
	'healthScore': 0.15000000000000002,
	'created_at': '2012-11-19 14:41:46.647 +0000',
	'deleted_at': '2014-04-02 21:42:53.000 +0000',
	'daysEmployed': 499,
	'daysLeft': 3109
}, {
	'user_id': '50927b7740aeba0200000005',
	'name': 'John Quinn',
	'healthScore': 0.15000000000000002,
	'created_at': '2012-11-01 13:39:03.336 +0000',
	'deleted_at': '2021-01-12 21:10:25.070 +0000',
	'daysEmployed': 2994,
	'daysLeft': 632
}, {
	'user_id': '5091e4beccbce30200000006',
	'name': 'Raphael Crawford-Marks',
	'healthScore': 0.31892096306348855,
	'created_at': '2012-11-01 02:55:58.441 +0000',
	'deleted_at': None,
	'daysEmployed': None,
	'daysLeft': None
}];