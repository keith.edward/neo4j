#Create your .env file
You can copy the .env template file

cp .env.template .env

The default values in the template are for your typical localhost 
environment, but you can update them to suit your needs.

#Install Docker

docker pull neo4j

#### If you want to save the database to your filesystem
    docker run \
    --detach \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --volume=$HOME/neo4j/logs:/logs \
    -v $HOME/neo4j/plugins:/plugins \
    --volume=$HOME/neo4j/conf:/conf \
    --env NEO4JLABS_PLUGINS='["graph-data-science"]' \
    --env NEO4J_AUTH=neo4j/test neo4j:4.4


create the file $HOME/neo4j/conf/neo4j.conf <br>
add the following contents <br>

dbms.tx_log.rotation.retention_policy=100M size
dbms.memory.pagecache.size=512M
dbms.default_listen_address=0.0.0.0
dbms.directories.plugins=/plugins
dbms.directories.logs=/logs
dbms.security.procedures.unrestricted=gds.*
dbms.security.procedures.allowlist=gds.*


# Import the Graph from Mongo DB

#### NodeJS
If you don't have Node JS installed, I suggest using Node Version Manager.
https://github.com/nvm-sh/nvm


#### Yarn
You will need Yarn https://classic.yarnpkg.com/lang/en/docs/install/#mac-stable

yarn install

yarn start


# Viewing the Graph
http://localhost:7474/browser/

See example-queries.txt for some example queries 
that you can get started with.

### Setup Python

brew install brew-pip



